﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMSOFT.EasyHotel.DAL;
using IEMSOFT.EasyHotel.DAL.Models;
using IEMSOFT.EasyHotel.Common;
using IEMSOFT.EasyHotel.Admin.Models;
using IEMSOFT.Foundation.MVC;
namespace IEMSOFT.EasyHotel.Admin.Controllers
{
    public class TravelAgencyController : BaseController
    {
        //
        // GET: /RoomType/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Get()
        {
            using (var db = new EasyHotelDB())
            {
                var dal = new TravelAgencyDAL(db);
                var travelAgencies = dal.GetList(CurrentUser.GroupHotelId);
                var model = AutoMapper.Mapper.Map<List<TravelAgencyModel>>(travelAgencies);
                return JsonNet(model);
            }
        }

        public ActionResult Option(string defaultText,string defaultValue)
        {
            List<SelectListItem> options;
            using (var db = new EasyHotelDB())
            {
                var dal = new TravelAgencyDAL(db);
                var ret = dal.GetList(CurrentUser.GroupHotelId);
                options = ret.Select((item) => new SelectListItem
                {
                    Text = item.Name,
                    Value = item.TravelAgencyId.ToString()
                }).ToList();
            }
            options.Insert(0, new SelectListItem { Text = "散客", Value = CommonConsts.Zero });
            if (!string.IsNullOrEmpty(defaultText))
            {
                options.Insert(0, new SelectListItem { Text = defaultText, Value = defaultValue??"" });
            }
            options[0].Selected = true;
            return JsonNet(options);
        }

        [HttpPost]
        public ActionResult Add(TravelAgencyModel model)
        {
            var ret = new BasicResult<string, List<string>>();
            ret.Msg = new List<string>();
            using (var db = new EasyHotelDB())
            {
                var dal = new TravelAgencyDAL(db);
                var existAgency = dal.GetOneByName(model.Name,CurrentUser.GroupHotelId);
                if (existAgency != null)
                {
                    ret.Msg.Add("该旅行社已经存在!");
                }
                else
                {
                    model.GroupHotelId = CurrentUser.GroupHotelId;
                    var dalModel = AutoMapper.Mapper.Map<TravelAgency>(model);
                    dal.Add(dalModel);
                }
            }
            return JsonNet(ret);
        }

        [HttpPost]
        public ActionResult update(TravelAgencyModel model)
        {
            var ret = new BasicResult<string, List<string>>();
            ret.Msg = new List<string>();
            using (var db = new EasyHotelDB())
            {
                var dal = new TravelAgencyDAL(db);
                model.GroupHotelId = CurrentUser.GroupHotelId;
                var dalModel = AutoMapper.Mapper.Map<TravelAgency>(model);
                dal.Save(dalModel);
            }
            return JsonNet(ret);
        }

        [HttpPost]
        public ActionResult Remove([FromJson]List<TravelAgencyModel> model)
        {
            var ret = new BasicResult<string, List<string>>();
            ret.Msg = new List<string>();
            using (var db = new EasyHotelDB(true))
            {
                using (var tx = db.DB.GetTransaction())
                {
                    var dal = new TravelAgencyDAL(db);
                    var dalModel = AutoMapper.Mapper.Map<List<TravelAgency>>(model);
                    dal.Remove(dalModel);
                    tx.Complete();
                }
            }
            return JsonNet(ret);
        }
    }
}
