﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMSOFT.EasyHotel.Admin.Models;
using IEMSOFT.Foundation.MVC;
using IEMSOFT.EasyHotel.Common;
namespace IEMSOFT.EasyHotel.Admin.Filters
{
    public class AuthorizeFilterAttribute : AuthorizeAttribute
    {
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            if (filterContext.HttpContext.Request.IsAjaxRequest())
            {
                var error = new BasicResult<string,List<string>>();
                error.Msg = new List<string>();
                error.Msg.Add("未授权操作，请登录后操作！");
                error.Data = StatusCode.Unauthenticated.ToString();
                var jsonResult = new JsonNetResult(error);
                filterContext.Result = jsonResult;
            }
            else
            {
                base.HandleUnauthorizedRequest(filterContext);
            }
        }
    }
}