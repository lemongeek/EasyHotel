﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IEMSOFT.EasyHotel.Admin.Models
{
    public class MonthReportSearchConditionModel
    {
        public string  FromYear { get; set; }
        public string FromMonth { get; set; }
        public string EndYear { get; set; }
        public string EndMonth { get; set; }
        public int? RoomTypeId { get; set; }
        public int? TravelAgencyId { get; set; }
        public int? PayTypeId { get; set; }
        public int? SubHotelId { get; set; }
    }
}