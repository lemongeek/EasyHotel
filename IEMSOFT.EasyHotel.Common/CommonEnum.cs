﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IEMSOFT.EasyHotel.Common
{
    public enum StatusCode
    {
        Success = 0,
        Failed = 1,
        UserNotExist = 2,
        PasswordInvalid = 3,
        PasswordMismatch = 4,
        Unauthenticated = 401,
        Exception = 500
    }

    public enum RoleType
    {
        None = 0,
        Reception = 1,
        HotelManager = 2,
        GroupLeader = 3
    }
    public enum RoomStatusType
    {
        Normal=1,
        CheckedIn=2,
        IsMending=3
    }

    public enum BillStatusType
    {
        Cancel=0,
        Finished=1,
        Pending=2        
    }
}
