﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IEMSOFT.EasyHotel.DAL
{
    public class EasyHotelDB : MySqlDBProvider
    {
        public EasyHotelDB()
            : base("EasyHotel")
        {
        }

        public EasyHotelDB(bool shouldKeepAlive)
            : base("EasyHotel", shouldKeepAlive)
        {

        }
    }
}
