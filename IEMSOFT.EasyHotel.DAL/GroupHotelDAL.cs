﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IEMSOFT.EasyHotel.DAL.Models;
namespace IEMSOFT.EasyHotel.DAL
{
  public  class GroupHotelDAL
    {
        private IDBProvider _dbProvider;
        public GroupHotelDAL(IDBProvider dbProvider)
        {
            _dbProvider = dbProvider;
        }

        public List<GroupHotel> GetList()
        {
            var ret = _dbProvider.DB.Fetch<GroupHotel>("select * from GroupHotel order by convert(Name USING gbk) COLLATE gbk_chinese_ci  asc ");
            return ret ?? new List<GroupHotel>();
        }

        public GroupHotel GetOne(int groupHotelId)
        {
            var ret = _dbProvider.DB.SingleOrDefault<GroupHotel>("select * from GroupHotel where GroupHotelId=@0",groupHotelId);
            return ret??new GroupHotel();
        }

        public void Save(GroupHotel groupHotel)
        {
            groupHotel.Modified = DateTime.Now;
            _dbProvider.DB.Save(groupHotel);
        }
    }
}
